# Automated testing with Gradle and JUnit

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![pipeline status](https://gitlab.com/NicholasLennox/gradle-ci/badges/master/pipeline.svg)](https://gitlab.com/NicholasLennox/gradle-ci/-/commits/master)

Project to show automated testing via a pipeline using a Gradle built system with Java.

To see steps needed to recreate the project, read through [Steps](STEPS.md).

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

No installation needed, all dependencies are downloaded through gradle.

## Usage

Open in Intellij and run. Requires Java 17.

## Maintainers

[@NicholasLennox](https://github.com/NicholasLennox)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Nicholas Lennox
